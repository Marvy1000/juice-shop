import requests
import sys

file_name = sys.argv[1]
scan_type = ""

if file_name == "gitleaks.json":
    scan_type = "Gitleaks Scan"
elif file_name == "njsscan.sarif":
    scan_type = "SARIF"
elif file_name == "semgrep.json":
    scan_type = "Semgrep JSON Report"
elif file_name == "retire.json":
    scan_type = "Retire.js Scan"
elif file_name == "trivy.json":
    scan_type = "Trivy Scan"

url = 'https://demo.defectdojo.org/api/v2/import-scan/'

my_headers = {'Authorization': 'Token bc7fb43d5a278794599b2d4f43570de55de0f26a'}    # demo site gets rebooted every hour, generate and update this token with new onw

# make sure new engagement is created or remove exising before using this
my_data = {
  "active": True,
  "verified": True,
  "scan_type": scan_type,
  "minimum_severity": "Low",
  "engagement": 18
}

# Add this to data: "engagement": 19 by creating engagement on UI and add the no a the end of its URL to my_data above

my_files = {
  "file": open(file_name, "rb")
}

response = requests.post(url, headers=my_headers, data=my_data, files=my_files)

if response.status_code == 201:
    print("Scan results imported successfully")
else:
    print(f"Failed to import scan results: {response.content}")
